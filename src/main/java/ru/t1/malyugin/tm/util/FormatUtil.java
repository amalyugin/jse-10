package ru.t1.malyugin.tm.util;

import java.text.DecimalFormat;

public interface FormatUtil {

    long KILOBYTE = 1024;
    long MEGABYTE = KILOBYTE * 1024;
    long GIGABYTE = MEGABYTE * 1024;
    long TERABYTE = GIGABYTE * 1024;

    static String formatBytes(long bytes) {
        if (bytes >= 0 && bytes < KILOBYTE) return floatForm(bytes) + " byte";
        if (bytes < MEGABYTE) return floatForm((double) bytes / KILOBYTE) + " Kb";
        if (bytes < GIGABYTE) return floatForm((double) bytes / MEGABYTE) + " Mb";
        if (bytes < TERABYTE) return floatForm((double) bytes / GIGABYTE) + " Gb";
        if (bytes >= TERABYTE) return floatForm((double) bytes / TERABYTE) + " Tb";

        return floatForm(bytes) + " byte";
    }

    static String floatForm(final double value) {
        return new DecimalFormat("#.##").format(value);
    }

}