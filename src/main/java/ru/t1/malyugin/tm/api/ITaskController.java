package ru.t1.malyugin.tm.api;

public interface ITaskController {
    void showTasks();

    void createTask();

    void clearTasks();

}