package ru.t1.malyugin.tm.api;

import ru.t1.malyugin.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}