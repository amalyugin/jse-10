package ru.t1.malyugin.tm.api;

public interface IProjectController {
    void showProjects();

    void createProject();

    void clearProjects();

}