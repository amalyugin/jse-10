package ru.t1.malyugin.tm.api;

import ru.t1.malyugin.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}