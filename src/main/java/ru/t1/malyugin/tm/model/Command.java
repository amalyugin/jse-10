package ru.t1.malyugin.tm.model;

public final class Command {

    private String name;

    private String argument;

    private String description;

    public Command(String name, String description, String argument) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public Command(String name, String description) {
        this.name = name;
        this.description = description;
        this.argument = null;
    }

    public Command() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        boolean isName = (name != null && !name.trim().isEmpty());
        boolean isArgument = (argument != null && !argument.trim().isEmpty());
        boolean isDescription = (description != null && !description.trim().isEmpty());

        result += (isName ? name + (isArgument ? ", " : "") : "");
        result += (isArgument ? argument : "");
        result += (isDescription ? " -> " + description : "");

        return result;
    }

}