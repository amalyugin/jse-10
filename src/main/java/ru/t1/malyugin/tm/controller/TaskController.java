package ru.t1.malyugin.tm.controller;

import ru.t1.malyugin.tm.api.ITaskController;
import ru.t1.malyugin.tm.api.ITaskService;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");

        final List<Task> tasks = taskService.findAll();
        if (tasks.isEmpty()) System.out.println("EMPTY");
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");

        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);

        if (task == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[REMOVE TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

}