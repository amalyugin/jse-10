package ru.t1.malyugin.tm.controller;

import ru.t1.malyugin.tm.api.IProjectController;
import ru.t1.malyugin.tm.api.IProjectService;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOW PROJECTS]");

        final List<Project> projects = projectService.findAll();
        if (projects.isEmpty()) System.out.println("EMPTY");
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");

        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);

        if (project == null) System.out.println("[FAILED]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[REMOVE PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

}