package ru.t1.malyugin.tm.repository;

import ru.t1.malyugin.tm.api.ICommandRepository;
import ru.t1.malyugin.tm.constant.ArgumentConst;
import ru.t1.malyugin.tm.constant.CommandConst;
import ru.t1.malyugin.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(CommandConst.HELP, "Show command list.", ArgumentConst.HELP);

    private static final Command VERSION = new Command(CommandConst.VERSION, "Show version info.", ArgumentConst.VERSION);

    private static final Command ABOUT = new Command(CommandConst.ABOUT, "Show Author info.", ArgumentConst.ABOUT);

    private static final Command INFO = new Command(CommandConst.INFO, "Show system info.", ArgumentConst.INFO);

    private static final Command PROJECT_LIST = new Command(CommandConst.PROJECT_LIST, "Show project list.");

    private static final Command PROJECT_CREATE = new Command(CommandConst.PROJECT_CREATE, "Create new project.");

    private static final Command PROJECT_CLEAR = new Command(CommandConst.PROJECT_CLEAR, "Clear all project.");

    private static final Command TASK_LIST = new Command(CommandConst.TASK_LIST, "Show task list.");

    private static final Command TASK_CREATE = new Command(CommandConst.TASK_CREATE, "Create new task.");

    private static final Command TASK_CLEAR = new Command(CommandConst.TASK_CLEAR, "Clear all tasks.");


    private static final Command EXIT = new Command(CommandConst.EXIT, "Close Application.");

    private static final Command[] COMMANDS = new Command[] {
            HELP, VERSION, ABOUT, INFO,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}